#version 330 core
in vec3 frag_col;
out vec3 color;

void main() {
	color = frag_col;
}
