#version 330 core
layout(location = 0) in vec3 vert_pos;
layout(location = 1) in vec3 vert_col;

uniform mat4 MVP;

out vec3 frag_col;

void main() {
	gl_Position = MVP * vec4(vert_pos, 1);

	frag_col = vert_col;
}
