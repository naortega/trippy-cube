/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#pragma once

#include "globals.h"

extern "C" {

#include <argp.h>
#include <stdlib.h>

const char *argp_program_version = VERSION;
const char *argp_program_bug_address = "nortega@themusicinnoise.net";

static char desc[] = "A psychedelic cube that changes color in 3D.";
static struct argp_option opts[] = {
	{ "fullscreen", 'f', 0, 0, "Set window to fullscreen", 0 },
	{ "screen-width", 'w', "width", 0, "Set width of resolution", 0 },
	{ "screen-height", 'h', "height", 0, "Set height of resolution", 0 },
	{ 0, 0, 0, 0, 0, 0 }
};

struct args {
	int fullscreen;
	unsigned short width, height;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct args *args = (struct args*)state->input;
	switch(key)
	{
		case 'f':
			args->fullscreen = 1;
			break;
		case 'w':
			args->width = (unsigned short)atoi(arg);
			break;
		case 'h':
			args->height = (unsigned short) atoi(arg);
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static struct argp argp = { opts, parse_opt, 0, desc, 0, 0, 0 };
}
