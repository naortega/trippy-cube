/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#include "camera.hpp"

#include <iostream>
#include <cmath>

camera::camera() : pos(0.0f, 0.0f, 0.0f),
		yaw(45.0f), angle(45.0f), dist(5.0f) { }

void camera::update(const input *in_sys) {
	std::pair<int, int> mm = in_sys->get_mouse_motion();
	angle -= glm::radians(static_cast<float>(mm.first));
	yaw += glm::radians(static_cast<float>(mm.second));
	std::get<0>(pos) = dist * cos(yaw) * sin(angle);
	std::get<1>(pos) = dist * sin(yaw);
	std::get<2>(pos) = dist * cos(yaw) * cos(angle);

	dist -= static_cast<float>(in_sys->get_scroll()) / 2.5f;
	if(dist < 0.0f)
		dist = 0.0f;
}
