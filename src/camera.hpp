/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#pragma once

#include <GL/gl.h>
#include <tuple>
#include <glm/gtc/matrix_transform.hpp>

#include "input.hpp"

class camera {
public:
	camera();
	void update(const input *in_sys);
	inline glm::vec3 get_pos() {
		return glm::vec3(std::get<0>(pos),
				std::get<1>(pos),
				std::get<2>(pos));
	}

private:
	std::tuple<GLfloat, GLfloat, GLfloat> pos;
	float yaw;
	float angle;
	float dist; // distance from origin
};
