/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#include "cube.hpp"

#include <cstdlib>
#include <ctime>

cube::cube() {
	glGenBuffers(1, &vbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vbuffer);
	glBufferData(GL_ARRAY_BUFFER,
			vertices.size() * sizeof(GLfloat),
			&vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &ibuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			indices.size() * sizeof(GLuint),
			&indices[0], GL_STATIC_DRAW);

	srand(static_cast<unsigned int>(time(0)));
	for(size_t i = 0; i < vertices.size(); ++i)
	{
		colors.push_back(static_cast<float>(rand()) /
				static_cast<float>(RAND_MAX));
		color_ascending.push_back(rand() % 2 ? true : false);
	}

	glGenBuffers(1, &cbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, cbuffer);
	glBufferData(GL_ARRAY_BUFFER,
			colors.size() * sizeof(GLfloat),
			&colors[0], GL_STATIC_DRAW);
}

cube::~cube() {
	glDeleteBuffers(1, &vbuffer);
	glDeleteBuffers(1, &ibuffer);
	glDeleteBuffers(1, &cbuffer);
}

void cube::render() {
	shift_colors();
	// setup vertex buffer
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, cbuffer);
	glBufferData(GL_ARRAY_BUFFER,
			colors.size() * sizeof(GLfloat),
			&colors[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuffer);
	glDrawElements(GL_TRIANGLES, indices.size(),
			GL_UNSIGNED_INT, nullptr);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void cube::shift_colors() {
	for(size_t i = 0; i < colors.size(); ++i)
	{
		colors[i] += color_ascending[i] ? color_shift :
			-color_shift;
		if(colors[i] >= 1.0f)
		{
			colors[i] = 1.0f;
			color_ascending[i] = false;
		}
		else if(colors[i] <= 0.0f)
		{
			colors[i] = 0.0f;
			color_ascending[i] = true;
		}
	}
}
