/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#pragma once

#include <vector>
#include <GL/glew.h>
#include <GL/gl.h>

class cube {
public:
	cube();
	~cube();

	void render();

private:
	const float color_shift = 0.005;
	void shift_colors();

	// OpenGL buffers
	GLuint vbuffer;  // vertex buffer
	GLuint ibuffer;  // index buffer
	GLuint cbuffer;  // color buffer
	// array of vertices
	const std::vector<GLfloat> vertices = {
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f
	};
	// index array to make triangles
	// NOTE: multiply the position x3 to get the position
	// in the `vertices' array
	const std::vector<GLuint> indices = {
		0, 1, 2,
		1, 3, 2,
		2, 3, 7,
		2, 7, 6,
		2, 6, 4,
		2, 4, 0,
		0, 5, 1,
		0, 4, 5,
		1, 5, 3,
		3, 5, 7,
		6, 7, 5,
		6, 5, 4
	};

	// color control
	std::vector<GLfloat> colors;
	std::vector<bool> color_ascending;
};
