/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#include "input.hpp"

#include <iostream>
#include <stdexcept>

input::input() : scroll(0), mmotion(0,0) {
	actions["quit"] = false;
	actions["move_left"] = false;
	actions["move_right"] = false;
	actions["move_foward"] = false;
	actions["move_backward"] = false;

	int mx, my;
	SDL_GetRelativeMouseState(&mx, &my);
	old_mpos = std::make_pair(mx, my);
}

void input::sync_events() {
	scroll = 0;
	SDL_Event e;
	while(SDL_PollEvent(&e))
	{
		switch(e.type)
		{
			case SDL_QUIT:
				actions["quit"] = true;
				break;
			case SDL_KEYDOWN:
				set_key(e.key.keysym, true);
				break;
			case SDL_KEYUP:
				set_key(e.key.keysym, false);
				break;
			case SDL_MOUSEWHEEL:
				if(e.wheel.y not_eq 0)
					scroll = e.wheel.y;
		}
	}

	// calculate mouse deltas
	int mx, my;
	SDL_GetRelativeMouseState(&mx, &my);
	mmotion.first = mx - old_mpos.first;
	mmotion.second = my - old_mpos.second;
}

bool input::get_action(const std::string &action) const {
	try {
		return actions.at(action);
	} catch(std::exception &e) {
		std::cerr << "Failed to access action `" << action <<
			"': " << e.what();
		return false;
	}
}

void input::set_key(SDL_Keysym key, bool value) {
	switch(key.sym)
	{
		case SDLK_w:
			actions["move_foward"] = value;
			break;
		case SDLK_s:
			actions["move_backward"] = value;
			break;
		case SDLK_a:
			actions["move_left"] = value;
			break;
		case SDLK_d:
			actions["move_right"] = value;
			break;
		case SDLK_ESCAPE:
		case SDLK_q:
			actions["quit"] = value;
		default:
			break;
	}
}
