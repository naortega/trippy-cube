/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#pragma once

#include <SDL2/SDL.h>
#include <map>
#include <utility>

class input {
public:
	input();
	void sync_events();
	bool get_action(const std::string &action) const;
	inline std::pair<int, int> get_mouse_motion() const {
		return mmotion;
	}
	inline int get_scroll() const {
		return scroll;
	}

private:
	void set_key(SDL_Keysym key, bool value);
	int scroll;
	std::map<std::string, bool> actions;
	std::pair<int, int> old_mpos; // old mouse position
	std::pair<int, int> mmotion; // mouse motion
};
