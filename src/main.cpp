/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#include <iostream>

#include <argp.h>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include "args.h"
#include "shaders.hpp"
#include "simulation.hpp"

unsigned short screen_width;
unsigned short screen_height;

SDL_Window *window;
GLuint program_id;
GLuint matrix_id;

int main(int argc, char *argv[]) {
	// handle arguments
	struct args args = { 0, 800, 600 };

	argp_parse(&argp, argc, argv, 0, 0, &args);

	screen_width = args.width;
	screen_height = args.height;

	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cerr << "ERROR: " << SDL_GetError() << std::endl;
		exit(1);
	}

	// setup SDL2 window
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
			SDL_GL_CONTEXT_PROFILE_CORE);
	window = SDL_CreateWindow("Trippy Cube",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			args.width, args.height,
			SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

	if(not window)
	{
		std::cerr << "ERROR: " << SDL_GetError() << std::endl;
		SDL_Quit();
		exit(1);
	}

	if(args.fullscreen)
		SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);

	SDL_SetRelativeMouseMode(SDL_TRUE);
	SDL_GLContext glcontext = SDL_GL_CreateContext(window);

	SDL_GL_SetSwapInterval(0);
	glewExperimental = true;
	glewInit();

	// create the VAO
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	// load the shaders into a GLSL program that can be run
	program_id = load_shaders("../shaders/vert_shader.glsl",
			"../shaders/frag_shader.glsl");
	matrix_id = glGetUniformLocation(program_id, "MVP");

	// set the clear color to black
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	run();

#ifdef DEBUG
	std::cout << "Shutting down..." << std::endl;
#endif
	glDeleteProgram(program_id);
	SDL_GL_DeleteContext(glcontext);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
