/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#include "shaders.hpp"

#include <fstream>
#include <stdexcept>
#include <sstream>
#include <vector>
#ifdef DEBUG
#	include <iostream>
#endif

GLuint load_shaders(const std::string &vertex_shader_path,
		const std::string &fragment_shader_path) {
	GLuint vshader_id = glCreateShader(GL_VERTEX_SHADER);
	GLuint fshader_id = glCreateShader(GL_FRAGMENT_SHADER);

	// load shaders from files
	std::string vshader_code;
	{
		std::ifstream vshader_stream(vertex_shader_path,
				std::ios::in);
		if(not vshader_stream.is_open())
		{
			throw std::runtime_error("Failed to open file " +
					vertex_shader_path);
		}
		std::stringstream ss;
		ss << vshader_stream.rdbuf();
		vshader_code = ss.str();
		vshader_stream.close();
	}

	std::string fshader_code;
	{
		std::ifstream fshader_stream(fragment_shader_path,
				std::ios::in);
		if(not fshader_stream.is_open())
		{
			throw std::runtime_error("Failed to open file " +
					fragment_shader_path);
		}
		std::stringstream ss;
		ss << fshader_stream.rdbuf();
		fshader_code = ss.str();
		fshader_stream.close();
	}

	GLint res = GL_FALSE;
	int info_log_length = 0;

	// compile and check shaders
#ifdef DEBUG
	std::cout << "Compiling shader: " << vertex_shader_path <<
		std::endl;
#endif
	{
		const char *vsp = vshader_code.c_str();
		glShaderSource(vshader_id, 1, &vsp, nullptr);
		glCompileShader(vshader_id);
	}

	glGetShaderiv(vshader_id, GL_COMPILE_STATUS, &res);
	glGetShaderiv(vshader_id, GL_INFO_LOG_LENGTH,
			&info_log_length);
	if(info_log_length > 0)
	{
		char *vshader_error = new char[info_log_length+1];
		glGetShaderInfoLog(vshader_id, info_log_length,
				nullptr, vshader_error);
		std::string error = "Shader compilation failed: ";
		error.append(vshader_error);
		delete vshader_error;

		throw std::runtime_error(error);
	}

#ifdef DEBUG
	std::cout << "Compiling shader: " << fragment_shader_path <<
		std::endl;
#endif
	{
		const char *fsp = fshader_code.c_str();
		glShaderSource(fshader_id, 1, &fsp, nullptr);
		glCompileShader(fshader_id);
	}

	glGetShaderiv(fshader_id, GL_COMPILE_STATUS, &res);
	glGetShaderiv(fshader_id, GL_INFO_LOG_LENGTH,
			&info_log_length);
	if(info_log_length > 0)
	{
		char *fshader_error = new char[info_log_length+1];
		glGetShaderInfoLog(fshader_id, info_log_length,
				nullptr, fshader_error);
		std::string error = "Shader compilation failed: ";
		error.append(fshader_error);
		delete fshader_error;

		throw std::runtime_error(error);
	}

	// link
#ifdef DEBUG
	std::cout << "Linking program..." << std::endl;
#endif
	GLuint program_id = glCreateProgram();
	glAttachShader(program_id, vshader_id);
	glAttachShader(program_id, fshader_id);
	glLinkProgram(program_id);

	// check linkage
	glGetProgramiv(program_id, GL_LINK_STATUS, &res);
	glGetProgramiv(program_id, GL_INFO_LOG_LENGTH,
			&info_log_length);
	if(info_log_length > 0)
	{
		char *link_error = new char[info_log_length+1];
		glGetProgramInfoLog(program_id, info_log_length,
				nullptr, link_error);
		std::string error = "Shader linking failed: ";
		error.append(link_error);
		delete link_error;

		throw std::runtime_error(error);
	}

	// detach
	glDetachShader(program_id, vshader_id);
	glDetachShader(program_id, fshader_id);

	glDeleteShader(vshader_id);
	glDeleteShader(fshader_id);

	return program_id;
}
