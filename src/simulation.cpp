/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

#include "simulation.hpp"

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL.h>

#include "globals.h"
#include "input.hpp"
#include "camera.hpp"
#include "cube.hpp"

void run() {
	input in_sys;
	camera cam;
	cube box;

	unsigned int last_time = SDL_GetTicks();

	while(not in_sys.get_action("quit"))
	{
		in_sys.sync_events();
		cam.update(&in_sys);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glUseProgram(program_id);

		glm::mat4 mvp;
		{
			glm::mat4 proj = glm::perspective(glm::radians(45.0f),
					static_cast<float>(screen_width) / static_cast<float>(screen_height),
					0.1f, 100.0f);

			glm::mat4 view = glm::lookAt(
					cam.get_pos(), // camera position
					glm::vec3(0,0,0), // where the camera is looking
					glm::vec3(0,1,0) // which way is vertically up
					);
			glm::mat4 mod = glm::mat4(1.0f); // identity matrix, object is at origin
			mvp = proj * view * mod;

		}
		glUniformMatrix4fv(matrix_id, 1, GL_FALSE, &mvp[0][0]);

		box.render();
		SDL_GL_SwapWindow(window);

		if(SDL_GetTicks() - last_time < (1000 / 60))
			SDL_Delay((1000 / 60) - (SDL_GetTicks() - last_time));
		last_time = SDL_GetTicks();
	}
}
